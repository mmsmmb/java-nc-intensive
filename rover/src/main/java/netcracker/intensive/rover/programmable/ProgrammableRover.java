package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.command.RoverCommand;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware{
    private RoverProgram program;
    private SimpleRoverStatsModule simpleRoverStatsModule;

    public ProgrammableRover(GroundVisor groundVisor, SimpleRoverStatsModule simpleRoverStatsModule) {
        super(groundVisor);
        this.simpleRoverStatsModule = simpleRoverStatsModule;
    }

    public void executeProgramFile(String file) {
        RoverCommandParser roverCommandParser = new RoverCommandParser(this, file);
        program = roverCommandParser.getProgram();
        //for(Iterator i = program.getCommands().iterator(); i.hasNext(); вместо итератора используем аналог foreach
        for (RoverCommand command : program.getCommands()){
            command.execute();
        }
    }

    public void move(){
        super.move();
        if((boolean)this.getSettings().get(RoverProgram.STATS)){
            simpleRoverStatsModule.registerPosition(getCurrentPosition());
        }
    }

    public Map<String,Object> getSettings(){
        return program.getSettings();
    }
}
