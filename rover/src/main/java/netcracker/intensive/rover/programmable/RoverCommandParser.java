package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.command.*;
import netcracker.intensive.rover.constants.Direction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class RoverCommandParser {

    private ProgrammableRover rover;
    private RoverProgram program;
    private String path;

    public RoverCommandParser(ProgrammableRover rover, String path) {
        this.path = path;
        this.rover = rover;
        program = new RoverProgram();
    }

    public RoverProgram getProgram() throws RoverCommandParserException{
        boolean isConfig = true;
        String str;
        try{
            BufferedReader reader = new BufferedReader(new FileReader(this.getClass().getResource(path).getFile()));
            RoverCommand command;
            while((str = reader.readLine()) != null){
                if(isConfig){
                    if(!str.equals(RoverProgram.SEPARATOR)){
                        readConfig(str);
                    }
                    else{
                        isConfig = false;
                    }
                }
                else{
                    command = readCommand(str);
                    if((boolean)program.getSettings().get(RoverProgram.LOG)){
                        program.commands.add(new LoggingCommand(command));
                    }
                    else{
                        program.commands.add(command);
                    }
                }
            }
            reader.close();
        } catch (RuntimeException e) {
            throw new RoverCommandParserException();
        } catch (IOException e) {
            throw new RoverCommandParserException();
        }
        return program;
    }

    private void readConfig(String str) {
        switch (str){
            case RoverProgram.LOG + " on":{
                program.settings.put(RoverProgram.LOG, true);
                break;
            }
            case RoverProgram.LOG + " off":{
                program.settings.put(RoverProgram.LOG, false);
                break;
            }
            case RoverProgram.STATS + " on":{
                program.settings.put(RoverProgram.STATS, true);
                break;
            }
            case RoverProgram.STATS + " off":{
                program.settings.put(RoverProgram.STATS, false);
                break;
            }
        }
    }

    private RoverCommand readCommand(String str) {
        String[] strA = str.split(" ");
        RoverCommand command = null;
        if(strA[0].equals("move")){
            command = new MoveCommand(this.rover);
        }
        if(strA[0].equals("turn")){
            command = new TurnCommand(this.rover, identifyDirection(strA[1]));
        }
        if(strA[0].equals("lift")){
            command = new LiftCommand(this.rover);
        }
        if(strA[0].equals("land")){
            command = new LandCommand(this.rover, createPoint(strA[1], strA[2]), identifyDirection(strA[3]));
        }
        return command;
    }

    private Direction identifyDirection(String str){
        Direction direction = null;
        switch(str.toLowerCase()){
            case "north":{
                direction = Direction.NORTH;
                break;
            }
            case "east":{
                direction = Direction.EAST;
                break;
            }
            case "south":{
                direction = Direction.SOUTH;
                break;
            }
            case "west":{
                direction = Direction.WEST;
                break;
            }
        }
        return direction;
    }

    private Point createPoint(String a, String b){
        Point point = new Point(0,0);
        point.setX(Integer.valueOf(a));
        point.setY(Integer.valueOf(b));
        return point;
    }
}
