package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.command.RoverCommand;

import java.util.*;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";

    protected Map<String, Object> settings;
    protected List<RoverCommand> commands;

    public RoverProgram(){
        settings = new HashMap<String, Object>();
        commands = new ArrayList<RoverCommand>();
    }

    public Map<String, Object> getSettings() {
        return Collections.unmodifiableMap(settings);
    }

    public List<RoverCommand> getCommands(){
        return commands;
    }

}
