package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.Direction;

public class Rover implements Turnable, Moveable, Liftable, Landable {
    private Point position;
    private Direction direction;
    private GroundVisor groundVisor;
    private boolean flown;



    public Rover(GroundVisor groundVisor) {
        this.groundVisor = groundVisor;
        position = new Point(0,0);
        direction = Direction.SOUTH;
        flown = false;

    }

    @Override
    public void turnTo(Direction dir){
        direction = dir;
    }

    @Override
    public void move(){
        Point nextPosition = shift();
        if(nextPosition != null){
            try {
                if (!groundVisor.hasObstacles(nextPosition)) {
                    position = nextPosition;
                }
            }
            catch (OutOfGroundException e){
                lift();
            }
        }
    }

    private Point shift() {
        if (position == null || direction == null){
            return null;
        }
        else{
            Point nextPosition = new Point(position);
            int temp = 1 ;
            switch (direction){
                case NORTH:{
                    temp = position.getY();
                    nextPosition.setY(--temp);
                    break;
                }
                case EAST:{
                    temp = position.getX();
                    nextPosition.setX(++temp);
                    break;
                }
                case SOUTH:{
                    temp = position.getY();
                    nextPosition.setY(++temp);
                    break;
                }
                case WEST: {
                    temp = position.getX();
                    nextPosition.setX(--temp);
                    break;
                }
            }
            return nextPosition;
        }
    }

    @Override
    public void lift() {
        flown = true;
        position = null;
        direction = null;
    }

    @Override
    public void land(Point nextPosition, Direction nextDirection) {
        try {
                if(!groundVisor.hasObstacles(nextPosition)){
                    position = nextPosition;
                    direction = nextDirection;
                    flown = false;
                }
        }
        catch (OutOfGroundException e) {
            //?
        }
    }

    public Point getCurrentPosition(){
        return position;
    }

    public boolean isAirborne() {
        return flown;
    }

    public Object getDirection() {
        return direction;
    }
}
