package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.constants.Direction;

public class LandCommand implements RoverCommand {
    Rover rover;
    Point position;
    Direction direction;

    public LandCommand(Rover rover, Point position, Direction direction) {
        this.rover = rover;
        this.position = position;
        this.direction = direction;
    }

    @Override
    public void execute() {
        rover.land(position, direction);
    }

    @Override
    public String toString() {
        return "Land at " +
                position.toString() +
                " heading " + direction.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LandCommand)) return false;

        LandCommand that = (LandCommand) o;

        return position.equals(that.position);

    }

    @Override
    public int hashCode() {
        return position.hashCode();
    }
}
