package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Rover;

public class LiftCommand implements RoverCommand {
    Rover rover;

    public LiftCommand(Rover rover) {
        this.rover = rover;
    }

    @Override
    public void execute() {
        rover.lift();
    }

    @Override
    public String toString() {
        return "Rover lifted";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiftCommand)) return false;

        LiftCommand that = (LiftCommand) o;

        return rover.equals(that.rover);

    }

    @Override
    public int hashCode() {
        return rover.hashCode();
    }
}
