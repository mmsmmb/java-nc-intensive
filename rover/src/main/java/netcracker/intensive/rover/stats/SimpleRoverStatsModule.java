package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;

import java.util.ArrayList;
import java.util.Collection;

public class SimpleRoverStatsModule implements RoverStatsModule {

    private ArrayList<Point> visitedPoints;

    public SimpleRoverStatsModule() {
        visitedPoints = new ArrayList<>();
    }

    @Override
    public void registerPosition(Point position) {
        if(!isVisited(position)){
            visitedPoints.add(position);
        }
    }

    @Override
    public boolean isVisited(Point position) {
        return visitedPoints.contains(position);
    }

    @Override
    public Collection<Point> getVisitedPoints() {
        return visitedPoints;
    }
}
