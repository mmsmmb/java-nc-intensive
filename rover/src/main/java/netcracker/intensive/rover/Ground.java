package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

public class Ground {
    int length;
    int width;
    private GroundCell[][] landscape;

    Ground(int width, int length){
        this.width = width;
        this.length = length;
        this.landscape = new GroundCell[width][length];
    }

    public void initialize(GroundCell... args) throws IllegalArgumentException {
        int size = length * width;
        if(args.length < size) {
            throw new IllegalArgumentException();
        }
        else {
            int k = 0;
            for(int i = 0; i < length; i++){
                for(int j = 0; j < width; j++){
                    landscape[j][i] = args[k];
                    k++;
                }
            }

        }

    }

    public GroundCell getCell(int x, int y) throws OutOfGroundException {
        if(x >= 0 && x < width && y >= 0 && y < length){
            return landscape[x][y];
        }
        else throw new OutOfGroundException();
    }
}
